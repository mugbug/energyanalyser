# EC206
Energy cost calculator
___
Installing requirements on Windows:
* Download PyQt4 .whl file [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyqt4). <br>
  obs: choose the correct .whl file according to your Python version and architecture.

  ![Python Shell](https://puu.sh/uOR9k/449e27712e.png)
  ![Wheel files](https://puu.sh/uORma/627fe91cdb.png)
  
* Then open cmd on the directory where the .whl file is and run `pip install pyqt4_wheel_file.whl`,
  changing `pyqt4_wheel_file` with the downloaded file name.
  
After that you're all set!
